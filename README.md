# Assumptions & Notes

- All front end work done in the `ada-test-frontend` folder using `create-react-app`.

- I assumed the daatabase would live on a seperate domain and are separate apps so I enabled CORS in the backend. I needed this locally espeically since even though the domain is the same, the ports were different which does throw a CORS error. Ideally CORS would be configured to only allow the one domain to make JS requests to the backend.

- As a user opens nodes we need to fetch the node details at that point. This is because as the dataset gets larger the operation could be quite expensive to try and fetch them ahead of time. I end up caching them as requests are made so that we aren't requesting the same data over and over. Ideally this could be done by a CDN with a TTL (time to live) in front of the API but for the purposes of this test I simulated that in code with no TTL.

- Since we can't trust any data that is being submitted in the search box or the database - we sanitize all strings coming from both sources before processing or injecting into the DOM. I ended up using a module since there can be quite a few edge cases to account for and I cannot guarantee what types of strings the application could encounter in the future. Also I didn't want to strip any less than or greater than symbols incase they were used intentionally (not as markup).

- I assumed that I would be delivering both the backend and frontend as a whole unit so I've packaged both of them in this repo to be delivered together. I also configured a package.json at the root to allow for the ability to start both the frontend and backend simultaneously.

- I made both the sidebar and content scrollable seperately as depending on the number of nodes and number of pieces of content - users may want to view them independantly without needing to scroll back up to make another selection from the sidebar.

# App Setup and Docs

### Requirements

- Node 12
- Yarn (can be installed via homebrew on Mac)

### Environment Variables

- REACT_APP_API - Specify endpoint for where database api lives

### Installation Insturctions

1. Run `yarn` command in both `ada-test-backend` and `ada-test-frontend` folders to install required node modules for frontend and backend
2. Run `yarn start` from root of the project (this will start the backend and frontend for you)
