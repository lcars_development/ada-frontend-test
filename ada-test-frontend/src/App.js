import "./App.css";
import React, { useState, useEffect } from "react";
import axiosInstance from "./axiosConfig.js";

import AppContext from "./contextProvider.js";
import Reducer from "./reducers/App";
import Sidebar from "./components/Sidebar";
import DetailScreen from "./components/DetailScreen";

function App() {
  const [appState, dispatch] = React.useReducer(Reducer, {
    nodeCache: {},
    selectedNode: [],
  });
  const [nodes, setNodes] = useState();

  useEffect(() => {
    axiosInstance.get(`/variables`).then((res) => {
      dispatch({ type: "SET_VARIABLES", variables: res.data });
    });
    axiosInstance.get(`/nodes`).then((res) => {
      setNodes(res.data);
    });
  }, []);

  return (
    <AppContext.Provider value={{ appState, dispatch }}>
      <div className="App">
        <Sidebar nodes={nodes}> </Sidebar>
        <DetailScreen selectedNodes={appState.selectedNode}></DetailScreen>
      </div>
    </AppContext.Provider>
  );
}

export default App;
