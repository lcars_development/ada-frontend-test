import "./DetailScreen.css";
import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";

import DetailScreenItem from "../DetailScreenItem";
import AppContext from "../../contextProvider.js";

function DetailScreen({ selectedNodes }) {
  const { appState } = useContext(AppContext);
  const [contents, setContents] = useState();

  useEffect(() => {
    if (selectedNodes) {
      const contentArr = [];

      /*
        If its a clicked node, use the content property. 
        If its nodes returned from a search, use title property.
      */

      selectedNodes.forEach((node) => {
        if (node.content) {
          node.content.forEach((content) => {
            content.nodeId = node.id;
            contentArr.push(content);
          });
        } else {
          contentArr.push({
            id: node.id,
            body: node.title,
            type: "text",
          });
        }
      });
      setContents(contentArr);
    }
  }, [selectedNodes]);

  return (
    <div className="detail-screen" aria-live={"polite"}>
      {contents &&
        contents.map((contentItem, index) => {
          return (
            <DetailScreenItem
              key={`detail-screen-item-${index}`}
              id={contentItem.nodeId}
              body={contentItem.body}
              type={contentItem.type}
              url={contentItem.url}
              variables={appState.variables}
              searchTerm={appState.searchTerm}
            />
          );
        })}
      {contents && contents.length === 0 && appState.searchTerm && (
        <div className={"no-results"}>
          No results found for "{appState.searchTerm}"
        </div>
      )}
      {contents && contents.length === 0 && !appState.searchTerm && (
        <div className={"no-results"}>
          Make a selection from the sidebar or search for a node
        </div>
      )}
    </div>
  );
}

DetailScreen.defaultProps = {
  selectedNodes: [],
};

DetailScreen.propTypes = {
  selectedNodes: PropTypes.array,
};

export default DetailScreen;
