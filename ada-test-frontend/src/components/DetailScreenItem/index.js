import "./DetailScreenItem.css";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import sanitizeHtml from "sanitize-html";

import variableReplace from "../../utilities/variableReplace.js";
import fallbackReplace from "../../utilities/fallbackReplace.js";

function DetailScreenItem({ id, type, body, url, variables, searchTerm }) {
  const [message, setMessage] = useState();

  useEffect(() => {
    /* 
      Sanitize string first before any processing. We could just run a replace on the greater than and less than characters, 
      but better to use a module that will capture all edge cases. We also shouldn't show the user that there was HTML markup in the 
      database - but we should log it so developers can see the issue.
    */

    let newString = sanitizeHtml(body);

    /* The sanitize package doesn't tell you if it stripped content, check to see if the string has changed, if it has we know there
       was markup and should alert the developers */
    if (body && newString !== body) {
      console.warn(`HTML detected in Node ID: ${id}`);
    }

    // If there is no body content or this is an image, don't bother processing any futher
    if (!newString || type === "image") return;

    // If this was a search - highlight the search terms (sanitized in Search component)
    if (
      searchTerm &&
      newString.toLowerCase().includes(searchTerm.toLowerCase())
    ) {
      const regex = new RegExp(`${searchTerm}`, "gi");
      newString = body.replace(
        regex,
        `<span class="search-highlight">${body.match(regex)}</span>`
      );
    }

    /* 
      Loop through string to find any variables - search is using titles 
      but if variables ever appear in the titles it would be useful to have this
    */
    variables.forEach((variable) => {
      // If variable id is found then process it
      if (newString.includes(variable.id)) {
        newString = variableReplace(variable, newString);
      }
    });

    /*
      Check string if any variables were unable to be found, 
      if so loop through each match and replace it with its fallback 
    */
    const matched = newString.match(/{(.*?)}/g);
    if (matched) {
      matched.forEach((match) => {
        newString = fallbackReplace(match, newString);
      });
    }

    setMessage(newString);
  }, [id, body, type, searchTerm, variables]);

  // If after sanitization there is no string just return early (we don't want to display empty content)
  if (!message && type !== "image") {
    return <></>;
  }

  /* Safe to use dangerouslySetInnerHTML since we sanitized the string first, 
    URLs should be safe since we are not using dangerouslySetInnerHTML for images
  */
  return (
    <div className="detail-screen-item">
      <span className="title">{type}</span>
      {type === "image" && (
        <span className="body">
          <img src={url} alt=""></img>
        </span>
      )}
      {type === "text" && (
        <span
          className="body"
          dangerouslySetInnerHTML={{ __html: message }}
        ></span>
      )}
    </div>
  );
}

DetailScreenItem.defaultProps = {
  id: null,
  type: null,
  body: null,
  url: null,
  variables: [],
  searchTerm: null,
};

DetailScreenItem.propTypes = {
  id: PropTypes.number,
  type: PropTypes.string,
  body: PropTypes.string,
  url: PropTypes.string,
  variables: PropTypes.array,
  searchTerm: PropTypes.string,
};

export default DetailScreenItem;
