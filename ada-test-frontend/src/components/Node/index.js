import "./Node.css";
import React, { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import axiosInstance from "../../axiosConfig.js";

import AppContext from "../../contextProvider.js";

function Node({ parent, isChild, node, activeNode, setActiveNode, depth }) {
  const { appState, dispatch } = useContext(AppContext);

  const [nodeActive, setNodeActive] = useState(false);
  const [sidebarId, setSidebarId] = useState();
  const [children, setChildren] = useState();
  const [activeSubNode, setActiveSubNode] = useState();

  const getFullNodeData = (id) => {
    // Instead of always fetching new data, see if its in the cache
    if (appState.nodeCache[id]) {
      return appState.nodeCache[id];
    }

    return axiosInstance.get(`/nodes/${id}`).then((res) => {
      dispatch({ type: "SET_CACHED_NODE", newNode: res.data[0] });
      return res.data[0];
    });
  };

  // Create unique ID for node - needed as nodes are repeated under other connections
  useEffect(() => {
    setSidebarId(`${parent}-${node.id}-${depth}`);
  }, [parent, node.id, depth]);

  useEffect(() => {
    setNodeActive(activeNode === sidebarId);
  }, [activeNode, sidebarId]);

  const selectNode = async (id) => {
    // Shallow node doesn't tell us if we have connections, get full data for node that was selected
    const selectedNodeData = await getFullNodeData(id);

    // Set selected node to display in detail view
    dispatch({ type: "SET_SELECTED_NODE", selectedNode: [selectedNodeData] });

    // If the detailed version of this node has connections - look those up
    if (selectedNodeData.connections) {
      const childrenNodeData = [];

      selectedNodeData.connections.forEach((connection) => {
        const childNodeData = getFullNodeData(connection);
        childrenNodeData.push(childNodeData);
      });

      // Retrieve all data before pushing to state
      Promise.all(childrenNodeData).then((res) => {
        setChildren(res);
        setActiveNode(`${parent}-${node.id}-${depth}`);
      });
    }
  };

  return (
    <span
      className={`node ${isChild ? "child" : ""}`}
      aria-expanded={nodeActive}
    >
      <button onClick={() => selectNode(node.id)}>{node.title}</button>
      {children && nodeActive && (
        <ul>
          {children.map((childNode, index) => {
            return (
              <li key={`nodelist-level${depth}-${index}`}>
                <Node
                  isChild={true}
                  isActive={nodeActive}
                  parent={node.id}
                  node={childNode}
                  depth={depth + 1}
                  activeNode={activeSubNode}
                  setActiveNode={setActiveSubNode}
                ></Node>
              </li>
            );
          })}
        </ul>
      )}
    </span>
  );
}

Node.defaultProps = {
  parent: null,
  isChild: false,
  node: null,
  activeNode: null,
  setActiveNode: null,
  depth: null,
};

Node.propTypes = {
  parent: PropTypes.number,
  isChild: PropTypes.bool,
  node: PropTypes.object,
  activeNode: PropTypes.string,
  setActiveNode: PropTypes.func,
  depth: PropTypes.number,
};

export default Node;
