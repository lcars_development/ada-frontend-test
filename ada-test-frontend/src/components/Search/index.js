import "./Search.css";
import React, { useState, useContext } from "react";
import sanitizeHtml from "sanitize-html";
import axiosInstance from "../../axiosConfig.js";

import AppContext from "../../contextProvider.js";

function Search() {
  const { dispatch } = useContext(AppContext);

  const [searchValue, setSearchValue] = useState("");

  const handleSubmit = (e, value) => {
    e.preventDefault();
    const sanitziedInput = sanitizeHtml(value);

    if (!sanitziedInput || sanitziedInput === "") {
      dispatch({
        type: "SET_SEARCH",
        selectedNode: [],
        searchTerm: sanitziedInput,
      });

      return;
    }

    axiosInstance
      .post(`/nodes/search`, { query: sanitziedInput })
      .then((res) => {
        if (res.data.length > 0) {
          dispatch({
            type: "SET_SEARCH",
            selectedNode: res.data,
            searchTerm: sanitziedInput,
          });
        } else {
          /* 
            If the user typed in something that returned 
            0 results then we clear current search from Detail Screen
          */
          dispatch({
            type: "SET_SEARCH",
            selectedNode: [],
            searchTerm: sanitziedInput,
          });
        }
      });
  };

  return (
    <div className="search">
      <form onSubmit={(e) => handleSubmit(e, searchValue)}>
        <label htmlFor="search" className="sr-only">
          Search the database for matching nodes
        </label>
        <input
          type="text"
          id="search"
          placeholder="Search"
          onChange={(e) => setSearchValue(e.target.value)}
        ></input>
        <input type="submit" value="Submit Search" />
      </form>
    </div>
  );
}

export default Search;
