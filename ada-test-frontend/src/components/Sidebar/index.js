import "./Sidebar.css";
import React, { useState } from "react";
import PropTypes from "prop-types";

import Search from "../Search";
import Node from "../Node";

function Sidebar({ nodes }) {
  const [activeNode, setActiveNode] = useState();

  return (
    <div className="sidebar">
      <Search />
      <ul className="nodeTree">
        {nodes &&
          nodes.map((node, index) => {
            return (
              <li key={`nodelist-level1-${index}`}>
                <Node
                  isActive={`${node.id}-${node.id}-1` === activeNode}
                  parent={node.id}
                  depth={1}
                  node={node}
                  activeNode={activeNode}
                  setActiveNode={setActiveNode}
                  isRoot={true}
                ></Node>
              </li>
            );
          })}
      </ul>
    </div>
  );
}

Sidebar.defaultProps = {
  nodes: [],
};

Sidebar.propTypes = {
  nodes: PropTypes.array,
};

export default Sidebar;
