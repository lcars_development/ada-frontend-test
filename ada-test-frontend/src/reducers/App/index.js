const SET_SELECTED_NODE = "SET_SELECTED_NODE";
const SET_CACHED_NODE = "SET_CACHED_NODE";
const SET_VARIABLES = "SET_VARIABLES";
const SET_SEARCH = "SET_SEARCH";

const Reducer = (state, action) => {
  switch (action.type) {
    case SET_SELECTED_NODE:
      return { ...state, selectedNode: action.selectedNode };
    case SET_CACHED_NODE:
      const newState = { ...state };
      newState.nodeCache[action.newNode.id] = action.newNode;
      return newState;
    case SET_VARIABLES:
      return { ...state, variables: action.variables };
    case SET_SEARCH:
      return {
        ...state,
        selectedNode: action.selectedNode,
        searchTerm: action.searchTerm,
      };
    default:
      return null;
  }
};

export default Reducer;
