const fallbackReplace = (match, string) => {
  const variableAndFallback = match.slice(1, -1).split("|");

  // Not doing a global replace here since I believe variable id's could have different fallbacks
  const regex = new RegExp(`{${variableAndFallback[0]}\\|.*?}`);

  return string.replace(
    regex,
    `<span class="highlight">${variableAndFallback[1]}</span>`
  );
};

export default fallbackReplace;
