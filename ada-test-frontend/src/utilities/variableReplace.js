const variableReplace = (variable, string) => {
  const regex = new RegExp(`{${variable.id}\\|.*?}`, "g");
  return string.replace(
    regex,
    `<span class="highlight">${variable.name}</span>`
  );
};

export default variableReplace;
